# Copyright (C) 2017 Corrado Maurini, Tianyi Li
#
# This file is part of FEniCS Gradient Damage.
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

from gradient_damage import *

# set_log_level(ERROR)

class ThinFilm(QuasiStaticGradentDamageProblem):

    def __init__(self, ndim):
        self.ndim = ndim
        QuasiStaticGradentDamageProblem.__init__(self)

    def prefix(self):
        return "thinfilm" + str(self.ndim) + "d"

    def set_user_parameters(self):
        p = self.parameters
        p.material.add("K", 1.)

        p.time.min = 0.75
        p.time.max = 2.
        p.time.nsteps = 20

        ell_e = 0.1
        p.material.K = p.material.E / ell_e**2.
        print("p.material.K = %f" % p.material.K)
        p.material.ell = 0.01
        p.material.Gc = 8*p.material.ell/3
        p.AM.max_iterations = 200
        p.AM.tolerance = 1e-5

        p.material.law = "AT1"
        p.post_processing.plot_alpha = False
        p.post_processing.save_alpha = True
        p.post_processing.save_u = True
        p.post_processing.save_Beta = False
        p.post_processing.save_V = True
        p.solver_alpha.method = "gpcg"

    def generate_save_dir(self):
        savedir = self.prefix() + "-" + "exp" + "/"
        self.create_save_dir(savedir)
        return savedir

    def define_materials(self):
        """
        Return list of materials that will be set in the model.
        """
        material1 = ThinFilmMaterial(self.parameters.material, "everywhere")
        return [material1]

# TractionBar
    # def eps0(self):
    #     """
    #     Inelastic strain, supposed to be isotropic
    #     """
    #     return 0.

    # def define_bc_u(self):
    #     value0 = ["0"] * self.dimension
    #     valuet = ["0"] * self.dimension
    #     valuet[0] = "t"
    #     bc_1 = DirichletBC(self.V_u, Constant(value0), "near(x[0], 0)")
    #     bc_2 = DirichletBC(self.V_u, Expression(valuet, t=0.0, degree=1), "near(x[0], 1)")
    #     return [bc_1, bc_2]
# /TractionBar

    def epse(self, u):
        """
        Elastic strain
        """
        Id = Identity(len(u))
        e1 = Constant([1.0, 0.0])
        e2 = Constant([0.0, 1.0])
        # return self.eps(u) - self._eps0 * outer(e1, e1)
        return self.eps(u) - self._eps0 * Id

    def define_mesh(self):
        if self.ndim == 1:
            mesh = UnitIntervalMesh(100)
        elif self.ndim == 2:
            mesh = RectangleMesh(Point(0., 0.), Point(1., .05), 300, 50)
        elif self.ndim == 3:
            return -1
        return mesh

    def define_bc_alpha(self):
        bc_1 = DirichletBC(self.V_alpha, Constant(0.), "near(x[0], 0)")
        bc_2 = DirichletBC(self.V_alpha, Constant(0.), "near(x[0], 1)")
        return [bc_1, bc_2]

if __name__ == "__main__":

    # Run a 2-d simulation
    problem = ThinFilm(2)
    problem.solve()
