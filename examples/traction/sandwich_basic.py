# Copyright (C) 2017 Corrado Maurini, Tianyi Li
#
# This file is part of FEniCS Gradient Damage.
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

from gradient_damage import *
from mshr import *
import matplotlib.pyplot as plt
import os.path
from subprocess import Popen, PIPE, check_output
import numpy as np
# set_log_level(ERROR)

class Sandwich(QuasiStaticGradentDamageProblem):

    def __init__(self, ndim):
        self.ndim = ndim
        QuasiStaticGradentDamageProblem.__init__(self)
        self.partial_elastic_energy_value = []
        self.partial_dissipated_energy_value = []

    def prefix(self):
        return "sandwich"

    def set_user_parameters(self):
        p = self.parameters
        # p.geometry.add("L", 1.)
        # p.geometry.add("H", .1)
        # p.geometry.add("delta", .05)
        x0=1.
        p.geometry.add("L", 1.*x0)
        p.geometry.add("delta", 0.005*x0)
        p.geometry.add("H", 0.05 * x0)
        p.geometry.add("h", 0.0005 * x0)

        p.material.add("Ee", 1.) # one of the 2params of the elastic material (assuming same nu)

        p.time.min = 1.0
        p.time.max = 4.
        p.time.nsteps = 50

        p.material.E = .063
        p.material.ell = 0.004709
        p.material.Gc = 8*p.material.ell/3
        p.AM.max_iterations = 200
        p.AM.tolerance = 1e-5

        p.material.law = "AT1"
        p.post_processing.plot_alpha = False
        p.post_processing.save_alpha = True
        p.post_processing.save_u = True
        p.post_processing.save_Beta = False
        p.post_processing.save_V = False
        p.solver_alpha.method = "gpcg"

    def generate_save_dir(self):
        data = self.parameters.to_dict()
        signature = hashlib.md5(json.dumps(data, sort_keys=True).encode('utf-8')).hexdigest()
        savedir = self.prefix() + "-" + "exp" + "/" + signature + "/"
        self.create_save_dir(savedir)
        return savedir

    def define_materials(self):
        """
        Return list of materials that will be set in the model.
        """
        fiber = ElasticMaterial(self.parameters.material, 1)
        matrix = GradientDamageMaterial(self.parameters.material, 2)
        return [fiber, matrix]

# TractionBar
    def eps0(self):
        """
        Inelastic strain, supposed to be isotropic
        """
        return 0.

# /TractionBar

    # def epse(self, u):
    #     """
    #     Elastic strain
    #     """
    #     Id = Identity(len(u))
    #     e1 = Constant([1.0, 0.0])
    #     e2 = Constant([0.0, 1.0])
    #     # return self.eps(u) - self._eps0 * outer(e1, e1)
    #     return self.eps(u) - self._eps0 * Id

    def define_mesh(self):
        # if self.ndim == 1:
        #     return -1
        # elif self.ndim == 2:
        geom = self.parameters.geometry
        H = geom.H
        L = geom.L
        h = geom.h
        delta = geom.delta
        #     top    = Rectangle(Point(-L/2,H/2),Point(L/2,H/2+delta))
        #     bottom = Rectangle(Point(-L/2,-H/2-delta),Point(L/2,-H/2))
        #     matrix = Rectangle(Point(-L/2,-H/2),Point(L/2,H/2))
        #     mesh = generate_mesh(top+matrix+bottom, 100)
        #     # mesh = CSGCGALMeshGenerator2D.generate(top+matrix+bottom, 100)
        #     plot(mesh)
        #     plt.savefig('mesh.png', bbox_inches='tight')
        # elif self.ndim == 3:
        #     return -1
        geofile = """
        // L = 1 in Julien's file
        // H = 1 in our normalization
        x0    = 1;

        L     = %g;
        delta = %g;
        H     = %g;
        h     = %g;

        Point(1) = {-L/2, H/2, 0, h};
        Point(2) = { L/2, H/2, 0, h};
        Point(3) = {-L/2, H/2-delta, 0, h};
        Point(4) = { L/2, H/2-delta, 0, h};
        Point(5) = {-L/2, -H/2+delta, 0, h};
        Point(6) = { L/2, -H/2+delta, 0, h};
        Point(7) = {-L/2, -H/2, 0, h};
        Point(8) = { L/2, -H/2, 0, h};

        Line(1) = {1, 3};
        Line(2) = {3, 4};
        Line(3) = {4, 2};
        Line(4) = {2, 1};

        Line(5) = {3, 5};
        Line(6) = {5, 6};
        Line(7) = {6, 4};

        Line(8) = {5, 7};
        Line(9) = {7, 8};
        Line(10) = {8, 6};

        Line Loop(1) = {1, 2, 3, 4};
        Line Loop(2) = {5, 6, 7, -2};
        Line Loop(3) = {8, 9, 10, -6};

        Plane Surface(1) = {1};
        Plane Surface(2) = {2};
        Plane Surface(3) = {3};

        Physical Surface(300) = {2};
        Physical Surface(301) = {1, 3};

        Physical Line (1201) = {8, 5, 1};
        Physical Line (1202) = {10, 7, 3};
        Physical Point (101) = {7};
                """ %(L, delta, H, h)

        fname="sandwich_orig"
        with open("scripts/"+fname+".geo", 'w') as f:
            f.write(geofile)

        cmd = 'gmsh {}.geo -2 -o meshes/{}.msh'.format(fname, fname)
        print(check_output([cmd], shell=True))  # run in shell mode in case you are not run in terminal

        cmd = 'dolfin-convert -i gmsh meshes/{}.msh meshes/{}.xml'.format(fname, fname)
        Popen([cmd], stdout=PIPE, shell=True).communicate()

        mesh = Mesh("meshes/sandwich_orig.xml")
        return mesh

    def set_mesh_functions(self):
        geom = self.parameters.geometry
        H = geom.H
        L = geom.L
        delta = geom.delta

        # class Top(SubDomain):
        #     def inside(self, x, on_boundary):
        #         return (between(x[1], (-L/2., L/2.)) and between(x[0], (H/2., H/2.+delta)))

        # class Bottom(SubDomain):
        #     def inside(self, x, on_boundary):
        #         return (between(x[1], (-L/2., L/2.)) and between(x[0], (-H/2., -H/2.-delta)))

        class Matrix(SubDomain):
            def inside(self, x, on_boundary):
                return ( between(x[0], (-L/2., L/2.)) and between(x[1], (-H/2., H/2.)) )

        class Fibers(SubDomain):
            def inside(self, x, on_boundary):
                return ( between(x[0], (-L/2., L/2.)) and (between(x[1], (-H/2., -H/2.+delta))) or between(x[1], (H/2.-delta, H/2.)) )

        # top    = Top()
        # bottom = Bottom()
        matrix = Matrix()
        fibers = Fibers()

        self.cells_meshfunction = MeshFunction("size_t", self.mesh, self.dimension)
        self.cells_meshfunction.set_all(0)

        self.boundary_meshfunction = MeshFunction("size_t", self.mesh, self.dimension - 1)
        self.boundary_meshfunction.set_all(1)

        # top.mark(self.cells_meshfunction, 1)
        # bottom.mark(self.cells_meshfunction, 1)
        matrix.mark(self.cells_meshfunction, 2)
        fibers.mark(self.cells_meshfunction, 1)

        # plot(self.cells_meshfunction)
        plt.savefig('meshes/cells.png', bbox_inches='tight')

        left  = CompiledSubDomain("near(x[0], -%f/2., 1.e-4)"%L)
        right = CompiledSubDomain("near(x[0],  %f/2., 1.e-4)"%L)

        left.mark(self.boundary_meshfunction, 1)
        right.mark(self.boundary_meshfunction, 2)

        # plot(self.boundary_meshfunction)
        plt.savefig('meshes/boundaries.png', bbox_inches='tight')


    def define_bc_alpha(self):
        geom = self.parameters.geometry
        L = geom.L
        bc_1 = DirichletBC(self.V_alpha, Constant(0.), "near(x[0], %f)"%(L/2.))
        bc_2 = DirichletBC(self.V_alpha, Constant(0.), "near(x[0], %f)"%(-L/2.))
        return [bc_1, bc_2]


    def define_bc_u(self):
        geom = self.parameters.geometry
        L = geom.L
        value0 = "0"
        # valuet = "0"
        valuet = "t"
        bc_1 = DirichletBC(self.V_u.sub(0), Constant(value0), "near(x[0], %f)"%(L/2.))
        bc_2 = DirichletBC(self.V_u.sub(0), Expression(valuet, t=0.0, degree=1), "near(x[0], %f)"%(-L/2.))
        return [bc_1, bc_2]

    def set_measures(self):
        """
        Here we assign the Measure to get selective integration on boundaries and bulk subdomain
        The Measure are defined using self.cells_meshfunction and self.exterior_facets_meshfunction
        """
        try:
            self.dx = Measure("dx")(subdomain_data=self.cells_meshfunction) # fiber: ds(1), right: ds(2)
        except Exception:
            self.dx = dx
        try:
            self.ds = Measure("ds")(subdomain_data=self.exterior_facets_meshfunction)
        except Exception:
            self.ds = ds

    def set_user_post_processing(self):
        # import pdb; pdb.set_trace()

        partial_elastic_energies = [material.elastic_energy_density(self.u, self.alpha)*self.dx(material.subdomain_id) for material in self.materials]
        partial_dissipated_energies = [material.dissipated_energy_density(self.u, self.alpha)*self.dx(material.subdomain_id) for material in self.materials]

        elastic_en_brittle = self.materials[0].elastic_energy_density(self.u, self.alpha)*self.dx(2)
        elastic_en_elastic = self.materials[1].elastic_energy_density(self.u, self.alpha)*self.dx(1)

        # import pdb; pdb.set_trace()
        self.partial_elastic_energy_value.append([self.t, assemble(elastic_en_brittle), assemble(elastic_en_elastic)])

        dissipated_en_brittle = self.materials[0].dissipated_energy_density(self.u, self.alpha)*self.dx(2)
        dissipated_en_elastic = self.materials[1].dissipated_energy_density(self.u, self.alpha)*self.dx(1)

        self.partial_dissipated_energy_value.append([self.t, assemble(dissipated_en_brittle), assemble(dissipated_en_elastic)])

        if self.parameters.post_processing.save_energies and self.comm_rank == 0:
                pl.savetxt(self.save_dir + "partial_energies_elastic.txt", pl.array(self.partial_elastic_energy_value), "%.5e")
                pl.savetxt(self.save_dir + "partial_energies_dissipated.txt", pl.array(self.partial_dissipated_energy_value), "%.5e")

        # dissipated_energy_value = assemble(partial_dissipated_energy for partial_dissipated_energy in partial_dissipated_energies)
        # for m in self.materials:
            # print(m.subdomain_id)
            # print(assemble(partial_elastic_energies[m.subdomain_id]))
        # [assemble(partial_elastic_energies[material.subdomain_id]) for material in self.materials]
        # self.print0("elastic_energy_value  %.3e" %(elastic_energy_value))
        # self.print0("dissipated_energy_value  %.3e" %(dissipated_energy_value))

        return

if __name__ == "__main__":

    # Run a 2-d simulation
    problem = Sandwich(2)
    problem.solve()
