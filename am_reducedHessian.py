# FEnics code  Variational Fracture Mechanics
#

# A static solution of the variational fracture mechanics problems using the regularization AT2
#
# author: corrado.maurini@upmc.fr
#
# date: 25/05/2013
#
from dolfin import *
from mshr import *
import sys, os, sympy, shutil, math
import numpy as np
from petsc4py import PETSc
# ------------------
# Parameters
# ------------------
set_log_level(ERROR) # log level
parameters.parse()   # read paramaters from command line
# set some dolfin specific parameters
parameters["form_compiler"]["optimize"]     = True
parameters["form_compiler"]["cpp_optimize"] = True
parameters["form_compiler"]["representation"] = "quadrature"

# parameters of the nonlinear solver used for the alpha-problem
solver_alpha_parameters =  {"method" : "gpcg", 
                            "linear_solver" : "cg", 
                            "preconditioner" : "bjacobi", 
                            "report" : False}
solver_u_parameters =  {"linear_solver" : "cg", 
                            "symmetric" : True, 
                            "preconditioner" : "hypre_amg", 
                            "krylov_solver" : {
                                "report" : False,
                                "monitor_convergence" : False,
                                "relative_tolerance" : 1e-8 
                                }
                            }# Geometry
L = 1.; H = .1; cell_size = .05
# Material constant
E, nu = Constant(10.0), Constant(0.3)
Gc = Constant(1.0)
ellv = 0.2/sqrt(2); ell = Constant(ellv)
k_ell = Constant(1.e-6) # residual stiffness
# Loading
ut = 1. # reference value for the loading (imposed displacement)
f = 0. # bulk load
load_min = 0. # load multiplier min value
load_max = 1. # load multiplier max value
load_steps = 50 # number of time steps
# Numerical parameters of the alternate minimization
maxiter = 200 
toll = 1e-5
# Constitutive functions of the damage model
def w(alpha):
    return alpha

def a(alpha):
    return (1-alpha)**2

modelname = "AT1"
# others
meshname = "mesh/bar-L%s-H%.2f-S%.4f.xdmf"%(L,H,cell_size)
regenerate_mesh = True
savedir = "results/%s-bar-L%s-H%.2f-S%.4f-l%.4f"%(modelname,L,H,cell_size,ellv)
if os.path.isdir(savedir):
    shutil.rmtree(savedir)
# ------------------
# Geometry and mesh generation
# ------------------
mesh = RectangleMesh(Point(0., 0.), Point(L, H), int(L/cell_size), int(H/cell_size))

# plot(mesh)
ndim = 2 # get number of space dimensions

PETScOptions().set("ksp_type", "preonly")
PETScOptions().set("pc_type", "cholesky")
PETScOptions().set("pc_factor_mat_solver_package", "mumps")
PETScOptions().set("mat_mumps_icntl_24", 1)
PETScOptions().set("mat_mumps_icntl_13", 1)



zero_v = Constant((0.,)*ndim)
e1 = [Constant([1.,0.]),Constant((1.,0.,0.))][ndim-2]

 # Strain and stress
def eps(v):
    return sym(grad(v))
    
def sigma_0(v):
    mu    = E/(2.0*(1.0 + nu))
    lmbda = E*nu/(1.0 - nu**2)
    return 2.0*mu*(eps(v)) + lmbda*tr(eps(v))*Identity(ndim)
    
# Normalization constant for the dissipated energy 
# to get Griffith surface energy for ell going to zero
z = sympy.Symbol("z")
c_w = 2*sqrt(2)*sympy.integrate(sympy.sqrt(w(z)),(z,0,1))

#----------------------------------------------------------------------------
# Define boundary sets for boundary conditions
#----------------------------------------------------------------------------
class Left(SubDomain):
    def inside(self, x, on_boundary):
        return near(x[0] * 0.01, 0)

class Right(SubDomain):
    def inside(self, x, on_boundary):
        return near((x[0] - L) * 0.01, 0.)

left = Left() 
right = Right()

boundaries = FacetFunction("size_t", mesh)
boundaries.set_all(0)
left.mark(boundaries, 1) # mark left as 1
right.mark(boundaries, 2) # mark right as 2
ds = Measure("ds")[boundaries] # left: ds(1), right: ds(2)
V_u = VectorFunctionSpace(mesh, "CG", 1)
V_alpha = FunctionSpace(mesh, "CG", 1)
u, du, v = Function(V_u), TrialFunction(V_u), TestFunction(V_u)
alpha, dalpha, beta = Function(V_alpha), TrialFunction(V_alpha), TestFunction(V_alpha)

element_u = VectorElement("CG", mesh.ufl_cell(), 1)
element_alpha = FiniteElement("CG", mesh.ufl_cell(), 1)
element_u_alpha = MixedElement([element_u, element_alpha])
_V_u_alpha = FunctionSpace(mesh, element_u_alpha)

import pdb; pdb.set_trace()

ownership = _V_u_alpha.dofmap().ownership_range()

alpha_0 = interpolate(Expression("0.0", degree=1), V_alpha) # initial (known) alpha
u_L = zero_v
u_R = Expression(("t","0."), t = 0., degree=1)

# bc - u (imposed displacement)
Gamma_u_0 = DirichletBC(V_u, u_L, boundaries, 1)
Gamma_u_1 = DirichletBC(V_u, u_R, boundaries, 2)
bc_u = [Gamma_u_0, Gamma_u_1]
# bc - alpha (zero damage)
Gamma_alpha_0 = DirichletBC(V_alpha, 0.0, boundaries, 1)
Gamma_alpha_1 = DirichletBC(V_alpha, 0.0, boundaries, 2)
bc_alpha = [Gamma_alpha_0, Gamma_alpha_1]

# Fenics forms for the energies
def sigma(u,alpha):
    return (a(alpha)+k_ell)*sigma_0(u)

body_force = Constant((0.,0.))

elastic_energy = 0.5*inner(sigma(u,alpha), eps(u))*dx
external_work = dot(body_force, u)*dx
dissipated_energy = Gc/float(c_w)*(w(alpha)/ell+ 0.5*ell*dot(grad(alpha), grad(alpha)))*dx 
total_energy = elastic_energy + dissipated_energy - external_work

# Weak form of elasticity problem
E_u = derivative(total_energy, u, v)
E_alpha = derivative(total_energy, alpha, beta)
E_alpha_alpha = derivative(E_alpha, alpha, dalpha)

# Writing tangent problems in term of test and trial functions for matrix assembly
E_du = replace(E_u,{u:du})
E_dalpha = replace(E_alpha,{alpha:dalpha})

# Variational problem for the displacement
problem_u = LinearVariationalProblem(lhs(E_du), rhs(E_du), u, bc_u)

# Variational problem for the damage (non-linear to use variational inequality solvers of petsc)
class DamageProblem(OptimisationProblem):

    def f(self, x):
        return assemble(total_energy)

    def F(self, b, x):
        pass

    def J(self, A, x):
        pass

    def form(self, A, b, x):
        #alpha.vector()[:] = x
        assemble_system(E_alpha_alpha, E_alpha, bc_alpha, A_tensor=A, b_tensor=b)

problem_alpha = DamageProblem()

# Set up the solvers
solver_u = LinearVariationalSolver(problem_u)
solver_u.parameters.update(solver_u_parameters)
#info(solver_u.parameters, True)
# solver_alpha = PETScTAOSolver()
# solver_alpha.parameters.update(solver_alpha_parameters)
lb = interpolate(Expression("0.", degree = 1), V_alpha) # lower bound, set to 0
ub = interpolate(Expression("1.", degree = 1), V_alpha) # upper bound, set to 1
#info(solver_alpha.parameters,True) # uncomment to see available parameters

problem_nl = NonlinearVariationalProblem(E_alpha, alpha, bc_alpha, E_alpha_alpha)

solver_alpha = NonlinearVariationalSolver(problem_nl)
snes_solver_parameters_bounds = {"nonlinear_solver": "snes",
                          "snes_solver": {"linear_solver": "mumps",
                                         #"linear_solver": "umfpack",
                                          "maximum_iterations": 100,
                                          "report": False,
                                          "line_search": "basic",
                                          #"method":"vinewtonrsls",
                                          "absolute_tolerance":1e-6, 
                                          "relative_tolerance":1e-6,
                                          "solution_tolerance":1e-6}}                                      
solver_alpha.parameters.update(snes_solver_parameters_bounds)
info(solver_alpha.parameters, True)
problem_nl.set_bounds(lb.vector(), ub.vector())



#  loading and initialization of vectors to store time datas
load_multipliers = np.linspace(load_min,load_max,load_steps)
energies = np.zeros((len(load_multipliers),4))
iterations = np.zeros((len(load_multipliers),2))
forces = np.zeros((len(load_multipliers),2))
file_alpha = File(savedir+"/alpha.pvd") # use .pvd if .xdmf is not working
file_u = File(savedir+"/u.pvd") # use .pvd if .xdmf is not working
# Solving at each timestep
alpha_error = Function(V_alpha)
for (i_t, t) in enumerate(load_multipliers):
    u_R.t = t*ut
    # Alternate mininimization
    # Initialization
    iter = 1; err_alpha = 1
    # Iterations
    while err_alpha>toll and iter<maxiter:
        solver_u.solve()
        # import pdb; pdb.set_trace()

        solver_alpha.solve()
        # solver_alpha.solve(problem_alpha, alpha.vector())
        alpha_error.vector()[:] = alpha.vector() - alpha_0.vector()
        err_alpha = np.linalg.norm(alpha_error.vector().array(), ord = np.Inf)
        # if mpi_comm_world().rank == 0:
            # print "Iteration:  %2d, Error: %2.8g, alpha_max: %.8g" %(iter, err_alpha, alpha.vector().max())
        alpha_0.assign(alpha)
        iter=iter+1
    lb.vector()[:] = alpha.vector()
#   Postprocess

    K = PETScMatrix(mpi_comm_world())
    asm = SystemAssembler(J, zeroslike_ualpha, bcs=bcs)
    asm.assemble(H)


    pc = PETSc.PC().create(mpi_comm_world())
    pc.setOperators(H.mat())
    pc.setType("cholesky")
    pc.setFactorSolverPackage("mumps")
    pc.setUp()
    Fm = pc.getFactorMatrix()
    (neg, zero, pos) = Fm.getInertia()

    iterations[i_t] = np.array([t,iter])
    elastic_energy_value = assemble(elastic_energy)
    surface_energy_value = assemble(dissipated_energy)
    if mpi_comm_world().rank == 0:
        print("\nEnd of timestep %d with load multiplier %g"%(i_t, t))
        print("\nElastic and surface energies: (%g,%g)"%(elastic_energy_value,surface_energy_value))
        print("-----------------------------------------")
    energies[i_t] = np.array([t,elastic_energy_value,surface_energy_value,elastic_energy_value+surface_energy_value])
    forces[i_t] = np.array([t,assemble(inner(sigma(u,alpha)*e1,e1)*ds(1))])
    file_alpha << (alpha,t) 
    file_u << (u,t) 
    np.savetxt(savedir+'/energies.txt', energies)
    np.savetxt(savedir+'/forces.txt', forces)
    np.savetxt(savedir+'/iterations.txt', iterations)

import matplotlib.pyplot as plt
def critical_stress():
    xs = sympy.Symbol('x')
    wx = w(xs); sx = 1/(E*H*a(xs));
    res = sympy.sqrt(2*(Gc*H/c_w)*wx.diff(xs)/(sx.diff(xs)*ell))
    return res.evalf(subs={xs:0})

def plot_stress():
    plt.plot(forces[:,0], forces[:,1], 'b-o', linewidth = 2)
    plt.xlabel('Displacement')
    plt.ylabel('Force')
    force_cr = critical_stress()
    plt.axvline(x = force_cr/(E*H)*L, color = 'grey', linestyle = '--', linewidth = 2)
    plt.axhline(y = force_cr, color = 'grey', linestyle = '--', linewidth = 2)

def plot_energy():
    p1, = plt.plot(energies[:,0], energies[:,1],'b-o',linewidth=2)
    p2, = plt.plot(energies[:,0], energies[:,2],'r-o',linewidth=2)
    p3, = plt.plot(energies[:,0], energies[:,3],'k--',linewidth=2)
    plt.legend([p1, p2, p3], ["Elastic","Dissipated","Total"])
    plt.xlabel('Displacement')
    plt.ylabel('Energies')
    force_cr = critical_stress()
    plt.axvline(x = force_cr/(E*H)*L, color = 'grey',linestyle = '--', linewidth = 2)
    plt.axhline(y = H,color = 'grey', linestyle = '--', linewidth = 2)

def plot_energy_stress():
    plt.subplot(211)
    plot_stress()
    plt.subplot(212)
    plot_energy()
    plt.savefig(savedir+'/energies_force.png')
    plt.show()

plot_energy_stress()
interactive()
