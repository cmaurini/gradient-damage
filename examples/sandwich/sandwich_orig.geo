// L = 1 in Julien's file
// H = 1 in our normalization
x0    = 1;

L     = 1*x0;
delta = 0.005*x0;
H     = 0.05 * x0;
h     = 0.001 * x0;

Point(1) = {-L/2, H/2, 0, h};
Point(2) = { L/2, H/2, 0, h};
Point(3) = {-L/2, H/2-delta, 0, h};
Point(4) = { L/2, H/2-delta, 0, h};
Point(5) = {-L/2, -H/2+delta, 0, h};
Point(6) = { L/2, -H/2+delta, 0, h};
Point(7) = {-L/2, -H/2, 0, h};
Point(8) = { L/2, -H/2, 0, h};

Line(1) = {1, 3};
Line(2) = {3, 4};
Line(3) = {4, 2};
Line(4) = {2, 1};

Line(5) = {3, 5};
Line(6) = {5, 6};
Line(7) = {6, 4};

Line(8) = {5, 7};
Line(9) = {7, 8};
Line(10) = {8, 6};

Line Loop(1) = {1, 2, 3, 4};
Line Loop(2) = {5, 6, 7, -2};
Line Loop(3) = {8, 9, 10, -6};

Plane Surface(1) = {1};
Plane Surface(2) = {2};
Plane Surface(3) = {3};

Physical Surface(300) = {2};
Physical Surface(301) = {1, 3};

Physical Line (1201) = {8, 5, 1};
Physical Line (1202) = {10, 7, 3};
Physical Point (101) = {7};