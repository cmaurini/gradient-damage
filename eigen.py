from dolfin import *

# Test for PETSc and SLEPc
if not has_linear_algebra_backend("PETSc"):
    print("DOLFIN has not been configured with PETSc. Exiting.")
    exit()

if not has_slepc():
    print("DOLFIN has not been configured with SLEPc. Exiting.")
    exit()

# Define mesh, function space
mesh = RectangleMesh(Point(0., 0.), Point(1., .1), 100, 10)
V = FunctionSpace(mesh, "Lagrange", 1)

# Define basis and bilinear form
u = TrialFunction(V)
v = TestFunction(V)
a = dot(grad(u), grad(v))*dx

# Assemble stiffness form
A = PETScMatrix()
assemble(a, tensor=A)

# Create eigensolver
eigensolver = SLEPcEigenSolver(A)

info(eigensolver.parameters, True)
# Set the matrix
# eigensolver.setOperators(self._K_mat_reduced, self._M_mat_reduced)

# Symmetric matrix and looking for THE smallest value
eigensolver.setProblemType(eigensolver.ProblemType.GHEP)
# eigensolver.setWhichEigenpairs(eigensolver.Which.SMALLEST_REAL)
eigensolver.setTarget(-.01)
eigensolver.setWhichEigenpairs(eigensolver.Which.TARGET_REAL)
eigensolver.setDimensions(1)
st = eigensolver.getST()
st.setType('sinvert')
st.setFromOptions()
st.view()
# Solving method
# eigensolver.setType(p.method)
# eigensolver.view()







# Compute all eigenvalues of A x = \lambda x
print("Computing eigenvalues. This can take a minute.")
eigensolver.solve()

nconv = self.eigensolver.get_number_converged()
# Extract largest (first) eigenpair
# r, c, rx, cx = eigensolver.get_eigenpair(0)
for i in range(nconv):
    r, c, rx, cx = self.eigensolver.get_eigenpair(i)
    self.print0("\033[1;36m    2nd stability: r, c, rx, cx: %.3e\033[1;m" %(r, c, rx, cx))

# print("Largest eigenvalue: ", r)

# Initialize function and assign eigenvector
u = Function(V)
u.vector()[:] = rx
