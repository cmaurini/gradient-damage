from dolfin import *

# Test for PETSc and SLEPc
if not has_linear_algebra_backend("PETSc"):
    print("DOLFIN has not been configured with PETSc. Exiting.")
    exit()

if not has_slepc():
    print("DOLFIN has not been configured with SLEPc. Exiting.")
    exit()

# Define mesh, function space
mesh = RectangleMesh(Point(0., 0.), Point(1., .1), 100, 10)
V = FunctionSpace(mesh, "Lagrange", 1)

# Define basis and bilinear form
u = TrialFunction(V)
v = TestFunction(V)
a = dot(grad(u), grad(v))*dx

# Assemble stiffness form
A = PETScMatrix()
assemble(a, tensor=A)

# Create eigensolver
eigensolver = SLEPcEigenSolver(A)

info(eigensolver.parameters, True)

eps = eigensolver.eps()

eps.setProblemType(eps.ProblemType.GHEP)
eps.setTarget(-.01)
eps.setWhichEigenpairs(eps.Which.TARGET_REAL)
eps.setDimensions(1)
st = eps.getST()
st.setType('sinvert')
st.setFromOptions()

st.setUp()

eps.setUp()
# ksp.setUp()
# pc.setUp()
eps.view()
st.view()


# Solving method
# eigensolver.setType(p.method)
# eigensolver.view()







# Compute all eigenvalues of A x = \lambda x
print("Computing eigenvalues. This can take a minute.")
eps.solve()

nconv = eigensolver.get_number_converged()
# Extract largest (first) eigenpair
# r, c, rx, cx = eigensolver.get_eigenpair(0)
for i in range(nconv):
    r, c, rx, cx = eigensolver.get_eigenpair(i)
    print("\033[1;36m    2nd stability: r, c: %.3e, %.3e,\033[1;m" %(r, c))

# print("Largest eigenvalue: ", r)

# Initialize function and assign eigenvector
u = Function(V)
u.vector()[:] = rx
